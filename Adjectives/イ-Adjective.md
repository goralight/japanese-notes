# イ-Adjective

The イ-Adjective is the "Native" adjectives for Japanese and feels the most natural. It is normally used for very basic or instant responses. For example, `あつい = Hot`. It is very quick to say and easily memorable. イ-adjectives end in `い` and we can use this to modify the meaning of the adjective.

Some examples of an adjective are:
```
おいしい : tasty
ちいさい : small, little
うつくしい : beautiful (natural beauty)
```
We can edit these adjectives by changing the ending to give a different meaning simply by dropping the `い`

---

## Negation

We can turn an adjective into an negation by dropping the `い` and replace it with `くない`

```
おいしくない : not tasty
ちいさくない : not small
うつくしくない : not beautiful
```

---

## Past Tense

We can make these past tense also, by dropping the `い` and replacing it with `かった`
```
おいしかった : was tasty
ちいさかった : was small
うつくしかった : was beautiful
```

---

## Negation & Past Tense

We can also do negation with past tense, by dropping the `い` in the `くない` and adding the `かった` to it.

```
おいしくなかった : was not tasty
ちいさくなかった : was not small
うつくしくなかった : was not beautiful
```

---

## Sentence Example

We can use this to describe items and people, for example:
```
わたしの はやくない くるまは あかかったです : My not fast car was red
その ほんは むずかしくない です : The book is not difficult
```

You can put the adjective before or after the noun. It will make it so `[RED noun]` or `[noun IS RED]`

---

## Conjunction

We can also chain multiple adjectives to describe a noun. This is done by adding `くて` and removing the `い`.

```
おいしくて ちいさくて うつくしくて : tasty and small and beautiful
```

chaining many `くて` can look childish however, so if you need to chain many, you should drop the `て` and on the 2nd to last adjective, add a `そして` after it

```
おいしく, ちいさく, そして うつくしい : tasty, small, and beautiful
```

---

## Nominalisation
Adding ~ness to a adjective - hotness, coldness, tastiness, bigness

This is done by replacing the `い` with a `さ` 

```
あつさ : hotness
おいしさ : tastiness
おおきさ : bigness
```

An example sentence would be: 

```
その　はこの　おもさは　もんだいです : the box's heaviness is a problem
```

## Adverbialisation

(Adverb)
Adding ~ly to an adjective - Hotly, coldly, tastily

This is done by replacing the `い` with a `く`

```
あつく : hotly
おいしく : tastily
おおきく : bigly
```

This is mainly used to describe verbs though, for example

quickly touched vs quick touch.

A few good examples of this is:

```
To become
~く なる : plain
~く　なります : polite

あつく　なる : to become hot
```

```
To make
~く　する : plain
~く　します : polite

あつく する : to make hot

わたしは　この　へやを　あたたかく　する : I make this room hot
```
