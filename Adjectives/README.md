# Adjectives
There are two forms of adjectives which follow different rules. The イ-adjective and the な-adjective.

For both adjectives - you can say "very" and "not very". This can be done by:

```
とても : positive adjective
あまり + negation adjective : not very

とても おいしい : very tasty
とても おいしかった : was very tasty
あまり おいしくない : not very tasty
あまり おいしくなかった : was not very tasty
```