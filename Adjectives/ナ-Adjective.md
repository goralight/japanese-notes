# ナ-Adjective

The ナ-Adjective is similar to its sister adjective except that it used for more abstract and complex adjectives. This is mainly due to these words coming from china. An example of a ナ-adjective would be: `らくだ` meaning comfortable. These adjectives end in `だ`, unless they are used in certain situations, such as being used as an actual adjective, the would be suffixed with `な` instead of `だ`.

This adjective follows the same conjugation for nouns, in that the `だ` here can behave like `です` and has all of the perks from this.

An example of the adjective are:

```
なまいきだ : cocky
せっきょくてきだ : active / assertive
らくだ : comfortable
べんりだ : convenient
ふべんだ : inconvenient

```

When using a な-Adjective to modify a noun / something / someone - the adjective should end in a `な` instead of a `だ`
```
けんきょだ : humble
けんきょな せんせい : humble teacher
くるまは　べんりです : car is convenient
```

---

## Negation

We can turn an な-adjective into a negation by dropping the な family 
character (at the end) and replacing it with `じゃ ない` or a 
`じゃ ありません`

```
せっきょくてきじゃ ないです : (it is) Not active
せっきょくてきじゃ ありません : Not active (polite)
ダークじゃ ないです : (it is) Not dark
ダークじゃ ありません : Not dark (polite)
```

---

## Past Tense

You are able to turn a な-adjective into past tense by turning the な from
the adjective into a `だった` or `でした`

```
せっきょくてきだった : was active
せっきょくてきでした : Was active (polite)
ダークだった : Was dark
ダークでした : Was dark (polite)
```

---

## Negation & Past Tense

You are able to make past and negative adjectives also, by using parts of the い-adjective and the な-adjective,
by making use of the `い` from the `じゃ ない`:

```
せっきょくてきじゃ なかったです : was not active
せっきょくてきじゃ ありませんでした : was not active (polite)
ダークじゃ なかったです : was not dark
ダークじゃ ありませんでした : was not dark (polite)
```

---

## Conjunction

You can chain multiple な-adjectives together, similar to the い-adjective - by dropping the last character in the adjective
and replacing it with a `で`. This can be used for both adjectives. for example:

```
い and い
な and い
い and な
な and な
```
all you need to do is change the modifier for the adjective you are using and use the correct character (`で` for `な` / `くて` for `い`)

```
かれは まともで おおきいです : he is normal decent and big
かのじょは ちいさくて せっきょくてきです : she is small and active
```

---

## Nominalisation

Similar to the い-adjective, it is adding ~ness to an adjective. You drop the `だ` for a `さ`

```
しんせんさ : fresh -> freshness
げんきさ : energy -> energyness
```

An example sentence would be:

```
この メロンパンの しんせんさ そして おいしさ よかったです : this melon bread's freshness and tastiness was good
```

---

## Adverbialisation

(Adverb)
Like the い-adjective, this is used to describe a verb, and to basically add ~ly to the end of the adjective
You drop the `だ` and replace it with `に`. This is just before the verb / action you are describing

```
らくに : comfortably
せっきょくてきに : actively
```


```
to  become
~に　なる : plain
~に　なります : polite
らくに なります : to become comfortable

to make
~に する : plain
~に します
らくに に する : to make comfortable

わたしは あなたを らくに します : I will make you comfortable
```

