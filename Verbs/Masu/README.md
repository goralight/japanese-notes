# ~ます stem

|    	|                      ます-buddy                     	|                  Meaning                 	|
|:--:	|:---------------------------------------------------:	|:----------------------------------------:	|
| 14 	|                   たべそうに  ない                  	| looks unlikely to eat / not going to eat 	|
| 13 	|                      たべそうだ                     	|    looks likely to eat / going to eat    	|
| 12 	|                      たべやすい                     	|                easy to eat               	|
| 11 	|                      たべにくい                     	|                hard to eat               	|
| 10 	|                       たべかた                      	|      how to eat / the way of eating      	|
|  9 	|         (noun with mobility) さんぽに  いく         	|                go to walk                	|
|  8 	|             たべに くる・たべに  きます             	|                come to eat               	|
|  7 	|            たべに いく・たべに  いきます            	|                 go to eat                	|
|  6 	|                       たべたい                      	|                want to eat               	|
|  5 	|                      たべながら                     	|            while eating, Verb2           	|
|  4 	|                      たべなさい                     	|      Eat \[please\] (soft imperative)      	|
|  3 	|                    たべませんか？                   	| why don't we eat? / why aren't we eating 	|
|  2 	|                   たべましょうか？                  	|               Shall we eat?              	|
|  1 	|                    たべましょう！                   	|                Let's eat!                	|
|  x 	| たべます・たべません・たべました ・たべませんでした 	|   eat / not eat / did eat / didn’t eat   	|

The ~ます stem is added to the end of the verb to give politeness and can be used to add negation, tense and such. Depending on the group, there are different
rules to follow.

| Group      | Rule                                         | Example | 
| ---------- | -------------------------------------------- | ------- |
| If Group 1 | Turn last letter to its い sister row        |あう = あいます / なる = なります|
| If Group 2 | Drop the る                                  |ねる = ねます / みる = みます|
| If Group 3 | drop the る and turn the letter to its い sister row |する = します / くる = きます|

## Negation - たべません
This applies to all groups. You can negate verbs similar to negating na-adjectives by turning the ます into ません.

|Hiragana|~ます stem|~ません stem|Meaning|
|--------|----------|-----------|-------|
|つくる|つくります|つくりません|To (not) Make|
|たべる|たべます|たべません|To (not) eat|
|うんてんする|うんてんします|うんてんしません|To (not) Drive|

## Past Tense - たべました
This applies to all groups. you can make verbs past tense by turning the ます into ました

|Hiragana|~ます stem|~ました stem|Meaning|
|--------|----------|-----------|-------|
|つくる|つくります|つくりました|Made|
|たべる|たべます|たべました|Ate|
|うんてんする|うんてんします|うんてんしました|Drove|

## Past negative - たべありませんでした

Exactly the same as the na-adjectives - you can use ありませんでした

|Hiragana|~ます stem|~ありませんでした stem|Meaning|
|--------|----------|-----------|-------|
|つくる|つくります|つくりありませんでした|Did not make|
|たべる|たべます|たべありませんでした|Did not eat|
|うんてんする|うんてんします|うんてんしありませんでした|Did not drive|

## Question - たべますか?
We can add a question to all forms of the verb. Example will just use positive, past, negative, and past negative

|Hiragana|~Stems|Meaning|
|--------|-----------|-------|
|つくる|つくりませんか|Did you not make (it)?|
|たべる|たべましたか|did you eat?|
|うんてんする|うんてんしありませんでしたか?|Did you not drive?|
|のむ|のみますか|Do you drink?|

## Let's - たべましょう / Shall we? - たべましょうか
We can give suggestion and encouragement for a verb also. For example:

```
Let's eat! - たべましょう 
Shall see it? - みましょうか?
```

This is useful for suggesting or implying an action to be done

```
しごとに いきましょう！ - Let's go to work
うちに ラーメンを たべましょうか? - Shall we eat ramen at home?
```

## Soft imperative / soft request - たべなさい
You drop the ます and add なさい

This is used for making a request which is normally asked by a higher up person. Eg; elder, teacher, parent, 

etc. Its not a hard request - but the listener should still act on it without resistence.

It is similar to the english's tone of voice and the context of the situation / whos speaking.

```
しごとに いきなさい - (please) Go to work
たべなさい - (please) Eat
```

Sounds like an order but isn't hard / harsh way of asking.
It is similar to being told to do something with a soft implied please.

## While [Two actions at the same time] - たべながら
You drop the ます and add ながら. you can also add a comma here too.

This is useful for when descriving the person doing two verbs at the same time.

The later verb is the primary action and the prefixed verb is the secondary action.

```
わたしは うんてんしながら、コーヒー を のみました - I drank coffee while driving / while driving, I drank coffee
```

Here, the primary action would be the drinking, and the speaker was driving while doing it.

You can use the other verb stems for the other verb like normal.
Paricles and such need to be used correctly. eg を for the object, に for the location, etc.

## Wanting to - たべたい
You drop the ます and add たい
This is used when the speaker says they want to do the action.

```
コーラ を のみたい - I want to drink Cola.
```

notice that it ends in a い. This means that the rules of an い-adjective apply.

| Hiragana           	| Meaning            	|
|--------------------	|--------------------	|
|コーラ を のみたい 	| I want to drink cola 	|
|コーラ を のみたかったです| I wanted to drink cola|
|コーラ を のみたくないです| I dont want a cola|
|コーラ を のみたくなかったです| I did not want a cola|
|コーラ を のみたくて うちに いきたくないです| I want to drink cola and I dont want to go home|
|あなたは コーラ を のみたいですか？|Do you want to drink cola|

## Verbs with direction / movement - たべに　いく / たべに　くる
You drop the ます and add the に to the verb followed by the いく / くる verb

This is used for indicating 'To go and do (verb)'

```
さかな　を　かいに　いきました - I went to buy fish
```

You can chain this up with more locations:

さかなを　かいに　おみせに　いきました - I went to the store to buy fish
しごとに 日本に きました - I came to japan to work

## How to / the manner off - たべかた
You drop the ます and add the かた

you will tend to have a particle before the verb

This is used for saying how to do a verb or to talk about the manner of which the verb was done.

```
おはしの つかいかた - How to use chopsticks
かのじょの はなしかた - her way of speaking
```

you can combo this up with adjectives

```
かのじょの たべかたは はやいです - her way of eating is fast (she eats fast)
```

and other verbs

かのじょは かんじの かきかたの しりません - she doesnt know how to write kanji

## Difficulty to / easy to - たべにくい / たべやすい
You drop the ます and add the にくい / やすい

This is used for indicating that the verb is easy to do or that there is difficulty in it.

notice that it ends in an i-adjective. Meaning tha the rules for the i-adjective apply here.

| Eating	| Positive       	| Negative           	|
|----------	|----------------	|--------------------	|
| Present 	| たべやすい     	| たべやすくない     	|
| Past     	| たべやすかった 	| たべやすくなかった 	|

| See   	| Positive     	| Negative         	|
|---------	|--------------	|------------------	|
| Present 	| みにくい     	| みにくくない     	|
| Past    	| みにくかった 	| みにくくなかった 	|

You can also use the politest version, like the i-adjective

| To ride  	| Present              	| Past                       	|
|----------	|----------------------	|----------------------------	|
| Negative 	| のりやすくありません 	| のりやすくありませんでした 	|

## Thinking - ~と　おもう
Although this isnt really a 'stem' it should be noted

と　おもう is technically a verb to what we can do is, add the と to the end of the sentence.

This will indicate to the listener that you "Think" in the sentence you just said
All the rules apply for the sentence / nouns / adjectives / verbs / etc

Make sure to put the だ in if it is a だ type word. (nouns / na-adjective)

```
しんせんだと　おもう - I think it is fresh
しんせんじゃないと　おもう - I think its not fresh
しんせんじゃなかったと　おもう - I think it was not fresh
// Notice the order here - the negation is on the adjective, not the と　おもう.

ほんだと　おもう - I think it's a book

わたしは　あたたかくて　つめたい　アサヒが　のみやすいと　おもいます - I think warm and cold Asahi is easy to drink.
```

If you say that you think of a verb, for example
```
わたしは かれが  おさけ を のむと おもう - I think he drink
```
You have to use the plain version of the verb, notice the のむ in the sentence.

## Looks like to / seems to / looks likely to / going to - たべそうだ

You drop the ます and add the そうだ

notice the だ, this means that you can use ナ-adjective forms for this.

This is used for indicating the verb is likely, or looks like its going to happen.
It looks like its going to X would be an example.

It is mainly based off of visual information.

looks like to rain - あめが ふりそうだ
Seems to eat - たべそうです
たべそうじゃ ない - not going to eat

Because of the ナ-adjective form, we are able to use these forms also, for tense positive, negative and politness

## looks unlike to / doesnt seem to / looks unlikely to / not going to - たべそうに　ない

You drop the ます and add the そうに　ない

notice the い - this means you can use い-adjective forms for this

similar to the そうだ buddy, but for the opposite

たべそうに ない - not likely to eat
あめが ふりそうに ない - it is unlikely to rain
しごとしそうに  なかったです - did not seem to work
