# Verbs
Verbs are doing/action words and are used to describe an action someone is doing, for example:

Walking, Driving, Eating, Speaking, etc.

Japanese has three groups of verbs depending on the letter before the `る` or if it is the last letter. These are the rules for the groups:

| Group   | Rule                                         | Example    |
| ------- | -------------------------------------------- | ---------- |
| Group 1 | Not ending in `る` & is a `あ・う・お` sound | あう / なる |
| Group 2 | Is a `い・え` sound                          | ねる / みる |
| Group 3 | Only two: する・くる                        | -          |
