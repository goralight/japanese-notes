# ~て stem

|   | て-Buddy         | Meaning                    |
|---|------------------|----------------------------|
| 4 | たべて　から     | After eating - obligatory / order arrangement |
| 3 | たべて　いる     | Eating - action / State verbs                 |
| 2 | たべて　ください | Please eat - polite request                 |
| 1 | たべて           | Eat and Verb2 / Please eat - casual request |

the て-Stem is mainly used for conjunctive and reasoning for verbs. Already seen the conjunctive form via the adjectives, for both ナ・イ.

あつくて - hot and ...

しんせんで fresh and ...

when figuring out the て-stem for each verb, we must see which group the verb belongs to, and then ends in.

![](https://i.imgur.com/BBcFQhK.png)

for example:

G1 - いう　→　いって　→　言って

G1 - いく　→　いって　→　行って

G1 - のむ　→　のんで　→　飲んで

G2 - たべる　→　たべて　→　食べて

G3 -　しごとする　→　しごとして　→　仕事して

It should be noted also that there is Old Japanese (O.J) where group one's method is not actually used and follows the masu form and adds a て to the end.

For example:

G1 - さけぶ →　さけびます　→　さけびて

But this is not really used anymore and you should use the rules indicated above.

## Eat and Verb2 - Please eat (casual request) - たべて

Just figure out the group of the verb.

There are two ways of using just て form after a verb. The first is to string together two verb in a sentence.

たべて　のみます - Eat and drink.

We have seen this before for the adjectives. It should be noted that the end of the sentence still controlls the manner and the tense and such of the sentence.

パン　を　たべて　ぎゅうにゅう　を　のみたい - I want to eat bread and drink milk

This means you can chain as many verbs you want, but the last one will still decide on the tence and the flow / mannerism of the setence.

The other way is when it ends the sentence, it can be a casual request.

あさごはん　を　たべて - eat breakfast (please)
This is mainly used for parents to their children and such or between friends for casual speak which invovles a request.

## Please eat (polite request) - たべて　ください

Add ください to the end of the verb after deciding on verb group.

similar to ending the sentence with て, if you end it with て　ください its a polite request. This is used for asking for something to a higher up and showing respect.

この　ばんごはん　を　たべて　ください - please eat this dinner

(この)  これらの　しんせんな　りんご　を　つかってください - please use these fresh apples

## Eating (present progressive) action / State verbs - たべて　いる

Add いる to the end of the verb once the group has been decided

To indicate that a verb is currently happening, we can use a present action verb.

This can somewhat link to adding "-ing" to the end of a verb:

のんで　いる - drinking

たべて　いる - Eating

しごとして　いる - Working

かって　いる - Buying

This indicates that the speaker is currently performing the action. Buying shopping, eating food, drinking water, etc. But it can also indicate status of the speaker.

かのじょは　ベンチに　すわって　いる - She is sitting on the bench.

This could also be translated to "she sits on the bench", but from an english point of view, the top translation relates more.


notice the ending of this, it is いる, which is also a verb.
This means that we can use verb rules for this, such as make it masu form or any other verb stems.

たべて　いる

たべて　いません (not eating)

たべて　いて　のみます - Eating and drinking (wordy)

たべて　のんで　います - would be better

## After eating (obligatory / order arrangement) - たべて　から

Add から to the end of the verb once the group has been decided.

This is used to indicate a flow or an order to verbs.

This is best linked to the English "After" or "Before" or "And then"

て  を  あらってから、 ごはん  を  たべなさい - Wash your hands before eating / after washing your hands, please eat (soft imperative)

Having lots of て　から after eachother can sound droney, similar to English - so it is best to chain it with て's and then finish on the grouping with て　から

コーヒー を いれて  あつい  水  を いれてから  ぎゅうにゅう  を  いれてください - Add coffee (and), hot water, and then add milk
