- [Syntax](#syntax)
  * [Particles](#particles)
  * [Honorifics](#honorifics)
    + [Naming](#naming)
      - [Family](#family)
  * [Personal pronouns](#personal-pronouns)
- [Simple Phrases](#simple-phrases)
- [List of words](#list-of-words)
  * [Numerals](#numerals)
  * [Days of the week](#days-of-the-week)
- [Proverbs](#proverbs)

## Syntax

### Particles

`は` : (Pronounced wa) The focus of the sentence is after this.
An example would be - if someone asked who you are.
```
わたしは ジョン : me is JOHN
```
The most important part of the sentence is your `ジョン` (your name). So using `は` focuses the listener to the name part.

---

`が` : The focus of the sentence is before this.
An example would be - if someone asked who is John
```
わたしが ジョン : ME is john
```
The most important part of the sentence is `わたし` (me). So using `が` focuses the listener to the me part.
This is also mainly used when the focus is on the "what" / "which" / "where" / etc
```
どの えきが か？ : Which station
```
Note the `が` - as the focus is on the which, not the station itself.

---

`~の` : of~ possession / (Something)'s
```
わたしの かばん です   : My bag
あなたの かばん です   : Your bag
かのじょの かばん です : Her bag
かれの かばん です     : his bag
ジョンの かばん です   : John's bag
```
When adding `の` as a prefix to yourself, or someone / something else - you indicate to the listener that you are talking about the things possession to another object / thing. `わたしの` turns from Me / I into Mine.

Objects can have posession too. 
```
はこの : Box's
その　はこの　おもさ : The box's heaviness
くつの はこ : Shoe's box (shoe box)
```
So can be used to describe the characteristics of someone and an object.
Can also be chained to form a link of possessions.
```
わたしの　せんせいの　ほん : My teacher's book
かのじょは　わたしの　ちちの　あねです : She is my father's big sister
ダンさんは わたしの ははの おとうの いぬの ともだちです : Dan is my mother's father's dog's friend
```

---

### Honorifics

---

#### Naming

san, kun, chan,Senpai, kōhai, Sensei, sama, Shi

##### Family


---

### Personal pronouns

| English        | Japanese                                                                                                                                                                                      |
|----------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| I (casual)     | Males: おれ,  Females: あたし, Older: わし                                                                                                                                                   |
| I (humble)     | Males: ぼく (Young / Students),<br/>Adult Male / Female: わたし,<br/>Very Formal: わたくし                                                                                                            |
| You (Impolite) | あなた, Mainly used when arguing / fighting/ blaming,<br/>don't use it when addressing someone in a polite manner<br/>_あなたが すきです - Is fine however_                                              |
| You (casual)   | Both Genders: おまえ,<br/>Both Genders (rough): あんた                                                                                                                                            |
| You (polite)   | Say their name! ジョンさん！サトさん！ (John-san / Sato-san)                                                                                                                                  |
| He             | He (Also means boyfriend): かれ,<br/>That (far) guy: あいつ,<br/>That (close) guy: そいつ,<br/>This guy: こいつ,<br/>  Just use their name to be safe                                                          |
| She            | She (also means girlfriend): かのじょ,<br/>eg: She is my girlfriend: かのじょは　わたしの　かのじょです,<br/>This is my girlfriend: このこは　わたしの　かのじょです, <br/>Just use their name to be safe |
| Who            | Who / Who are you: だれ,<br/>Who are you (polite): だれ ですか？,<br/>Who are you(Cautious): おまえは　だれだ,<br/>What is your name: なまえ なに? / なまえ なんですか？                                   |
| Who (polite)   | Who: どちら / どなた,<br/>Honorific title: さま<br/>Who are you?: どちら(さま)ですか？ / どなたですか？                                                                                                |


---

## Simple Phrases

```
おひさしぶりです! : It has been a while (Long time no see)
いかが　おすごし でしたか? : How have you been?
そちらは　いま、なんじですか? : What time is it now?
```

---

## List of words

---

### Numerals

Generally, use the `Sino Counter`.

|   |  Native Counter | Sino Counter |
|----|----|---|
| 1  |ひ  | いち|
| 2  |ふ  | に|
| 3  |み  |さん |
| 4  |よ  |し / **よん** |
| 5  |いつ|ご |
| 6  |む  |ろく |
| 7  |なな|しち / **なな**|
| 8  |や  |はち |
| 9  |ここ|く / **きゅう** |
| 10 |とお|じゅう |

In order to count in tens - just add `じゅう` before another number to make it `10 + x`.  
eg  
`じゅう なな` is 17

To change the tenth unit, just prefix it with a number - `y10 + x`.  
eg  
`ろくじゅう いち` is 61

With this, you can make any number up to 99.

`ひゃく` is 100 - the same formula applies.  

`にひゃく よんじゅう きゅう` is 249

**_When writing - try and combine the units together - 4hundred 7ten 3unit_**  
**_This is for speaking really, use decimal numbers generally when writing_**

---

## Proverbs

**いしの　うえにも　さんねん**  
_Perseverance prevails_  
Top of stone for 3 years  
![](https://freeter-life.com/wp-content/uploads/2015/10/ishinoue.jpg)
```
いしの   : Of Stone
うえ     : On top / above / on
にも     : even on
さんねん : Three years
```

---

**えんの　したの　ちからもち**  
_Unsung heros / Nameless heros_  
Powerful people helping unseen
```
えんのした : Supports of Japanese home (えんがわ) - you cant see them
ちからもち : Powerful people
```
---

**うまの　みみに　ねんぶつ**  
_Preaching to the wind / Advice to deaf ears_  
Buddhist chant to horses ears
```
うまの　みみに : horses ears (to listen to)
ねんぶつ      : Buddhist chant
```

---

**おにに かなぼう**   
_Adding wings to a tiger / The more Moors, the better the victory_   
Oni With a club
```
おに       : Demon troll
(おに)に   : Oni (to use)
かなぼう   : Big club
```

---

**さるも　きから　おちる**  
_No matter how good you are, you can still fail_  
Monkey drops from the tree
```
さる   : monkey
さるも : monkey also
き    : tree
きから : tree (from)
おちる : to drop
```

---

**ならうより なれよ**  
_Practice makes perfect_  
Get used to rather than learn
```
ならう : to learn
より   : than
なれる : to get used to
```

---

**ほとけの　かおも　さんどまで**  
_Even god only forgives 3 times / Even the patience of a saint has its limits_  
Buddha only forgives three times
```
ほとけ : Buddha
かお   : Face (In this context also means forgiveness / generosity)
さんど : Three times
```
---
