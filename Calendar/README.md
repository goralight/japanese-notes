# Days of the week

|Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday
|---|---|---|---|---|---|---|
|げつようび|かようび|すいようび|もくようび|きんようび|どようび|にちようび
|月曜日|火曜日|水曜日|木曜日|金曜日|土曜日|日曜日

```
きょう     : Today
ようび     : day of the week
しゅうまつ : weekend  
あした     : tomorrow
きのう     : yesterday
```

eg:

```
きょうは　なんようび ですか？ : What day is today?  
きょうは　[Day]です          : Today is [Day]

あしたは なんようび ですか？  : What day is tomorrow?
あしたは [Day]です          : Tomorrow is [Day]

きのうは　なんようびでしたか? : What day was yesterday?
きのうは　[day]でした     : Yesterday was [day] 
```