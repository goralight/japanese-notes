# Adverbs

Like English, adverbs can be placed before a verb to give extra meaning:

Here a list of useful adverbs:

| English             	| Hiragana 	| Kanji 	|
|---------------------	|----------	|-------	|
| Always              	| いつも   	| -     	|
| Often               	| よく     	| -     	|
| Usually             	| たいてい 	| -     	|
| Sometimes           	| ときどき 	| 時々  	|
| Rarely              	| たまに   	| -     	|
| Not at all (w/ neg) 	| ぜんぜん 	| 全然  	|
| probably, maybe     	| たぶん   	| 多分  	|
| A lot (amount)      	| たくさん 	| -     	|
| A little (amount)   	| すこし   	| 少し  	|

