# Nouns

Although not visable, all nouns actually end with だ. This だ Is used to indicate "Is". This is why when conjugating it over to です in a sentence it would transalte to "is [noun]"

These are the set of conjugation rules. 

|                  	| Plain                                	| Casual                                       	| Polite                                               	|
|------------------	|--------------------------------------	|----------------------------------------------	|------------------------------------------------------	|
| Present Positive 	| 本(だ)                               	|                                              	| 本です                                               	|
| Past Positive    	| 本だった                             	|                                              	| 本でした                                             	|
| Present Negative 	| 本じゃない<br>本ではない         	| 本じゃないです<br>本ではないです     	| 本じゃありません<br>本ではありません             	|
| Past Negative    	| 本じゃなかった<br>本ではなかった 	| 本じゃなかったです<br>本ではなかったです 	| 本じゃありませんでした<br>本ではありませんでした 	|

These noun conjugations follow the same rules as the ナ-adjectives.


<!-- 
## Template

| English | ひらがな | かんじ |
| ------- | -------- | ------ |
|         |          |        | -->

